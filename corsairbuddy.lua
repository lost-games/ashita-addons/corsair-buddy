--[[
* Addons - Copyright (c) 2023 Ashita Development Team
* Contact: https://www.ashitaxi.com/
* Contact: https://discord.gg/Ashita
*
* This file is part of Ashita.
*
* Ashita is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Ashita is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Ashita.  If not, see <https://www.gnu.org/licenses/>.
--]]

addon.name = 'Corsair Buddy';
addon.author = 'Arkevorkhat';
addon.version = '1.0';
addon.desc = 'Gives additional data to assist with corsair rolls.';


local SnakeEyeActive = false;
local ActiveRolls = {};
local LuckyNumbers = {
    ['Corsair'] = {
        ['Lucky'] = '5',
        ['Unlucky'] = '9',
        ['Suffix'] = '\'s'
    },
    ['Ninja'] = {
        ['Lucky'] = '4',
        ['Unlucky'] = '8',
        ['Suffix'] = '\'s'
    },
    ['Hunter'] = {
        ['Lucky'] = '4',
        ['Unlucky'] = '8',
        ['Suffix'] = '\'s'
    },
    ['Chaos'] = {
        ['Lucky'] = '4',
        ['Unlucky'] = '8',
        ['Suffix'] = ''
    },
    ['Magus'] = {
        ['Lucky'] = '2',
        ['Unlucky'] = '6',
        ['Suffix'] = '\'s'
    },
    ['Healer'] = {
        ['Lucky'] = '3',
        ['Unlucky'] = '7',
        ['Suffix'] = '\'s'
    },
    ['Drachen'] = {
        ['Lucky'] = '4',
        ['Unlucky'] = '8',
        ['Suffix'] = ''
    },
    ['Choral'] = {
        ['Lucky'] = '2',
        ['Unlucky'] = '6',
        ['Suffix'] = ''
    },
    ['Monk'] = {
        ['Lucky'] = '3',
        ['Unlucky'] = '7',
        ['Suffix'] = '\'s'
    },
    ['Beast'] = {
        ['Lucky'] = '4',
        ['Unlucky'] = '8',
        ['Suffix'] = ''
    },
    ['Samurai'] = {
        ['Lucky'] = '2',
        ['Unlucky'] = '6',
        ['Suffix'] = ''
    },
    ['Companion'] = {
        ['Lucky'] = '2',
        ['Unlucky'] = '10',
        ['Suffix'] = '\'s'
    },
    ['Evoker'] = {
        ['Lucky'] = '5',
        ['Unlucky'] = '9',
        ['Suffix'] = '\'s'
    },
    ['Rogue'] = {
        ['Lucky'] = '5',
        ['Unlucky'] = '9',
        ['Suffix'] = '\'s'
    },
    ['Warlock'] = {
        ['Lucky'] = '4',
        ['Unlucky'] = '8',
        ['Suffix'] = '\'s'
    },
    ['Fighter'] = {
        ['Lucky'] = '5',
        ['Unlucky'] = '9',
        ['Suffix'] = '\'s'
    },
    ['Puppet'] = {
        ['Lucky'] = '3',
        ['Unlucky'] = '7',
        ['Suffix'] = ''
    },
    ['Gallant'] = {
        ['Lucky'] = '3',
        ['Unlucky'] = '7',
        ['Suffix'] = '\'s'
    },
    ['Wizard'] = {
        ['Lucky'] = '5',
        ['Unlucky'] = '9',
        ['Suffix'] = '\'s'
    },
    ['Dancer'] = {
        ['Lucky'] = '3',
        ['Unlucky'] = '7',
        ['Suffix'] = '\'s'
    },
    ['Scholar'] = {
        ['Lucky'] = '2',
        ['Unlucky'] = '6',
        ['Suffix'] = '\'s'
    },
    ['Naturalist'] = {
        ['Lucky'] = '3',
        ['Unlucky'] = '7',
        ['Suffix'] = '\'s'
    },
    ['Runeist'] = {
        ['Lucky'] = '4',
        ['Unlucky'] = '8',
        ['Suffix'] = '\'s'
    },
    ['Bolter'] = {
        ['Lucky'] = '3',
        ['Unlucky'] = '9',
        ['Suffix'] = '\'s'
    },
    ['Caster'] = {
        ['Lucky'] = '2',
        ['Unlucky'] = '7',
        ['Suffix'] = '\'s'
    },
    ['Courser'] = {
        ['Lucky'] = '3',
        ['Unlucky'] = '9',
        ['Suffix'] = '\'s'
    },
    ['Blitzer'] = {
        ['Lucky'] = '4',
        ['Unlucky'] = '9',
        ['Suffix'] = '\'s'
    },
    ['Tactician'] = {
        ['Lucky'] = '5',
        ['Unlucky'] = '8',
        ['Suffix'] = '\'s'
    },
    ['Allies'] = {
        ['Lucky'] = '3',
        ['Unlucky'] = '10',
        ['Suffix'] = '\''
    },
    ['Miser'] = {
        ['Lucky'] = '5',
        ['Unlucky'] = '7',
        ['Suffix'] = '\'s'
    },
    ['Avenger'] = {
        ['Lucky'] = '4',
        ['Unlucky'] = '8',
        ['Suffix'] = '\'s'
    }
}

ashita.events.register('text_in', 'handleModifyString', function (evt)
    if evt.injected or evt.blocked then return end
    if string.find(evt.message, 'Roll') == nil then return end;
    local foundRoll = ''
    for k, _ in pairs(LuckyNumbers) do
        if string.find(evt.message, k) ~= nil then
            foundRoll = k;
        end
    end
    if string.find(evt.message, 'Bust') ~= nil then
        evt.message_modified = string.format( "[%s%s Roll] - BUST!",foundRoll, LuckyNumbers[foundRoll].Suffix );
        return;
    end
    local roll = string.match( evt.message, '%d%d?' )
    if LuckyNumbers[foundRoll].Lucky == roll then
        evt.message_modified = string.format('[%s%s Roll] .-%s-. Lucky!', foundRoll, LuckyNumbers[foundRoll].Suffix, roll);
    elseif LuckyNumbers[foundRoll].Unlucky == roll then
        evt.message_modified = string.format('[%s%s Roll] .-%s-. Unlucky...', foundRoll, LuckyNumbers[foundRoll].Suffix, roll);
    else
        evt.message_modified = string.format('[%s%s Roll] .-%s-. Lucky: %s - Unlucky %s', foundRoll, LuckyNumbers[foundRoll].Suffix, roll, LuckyNumbers[foundRoll].Lucky, LuckyNumbers[foundRoll].Unlucky);
    end
end
)